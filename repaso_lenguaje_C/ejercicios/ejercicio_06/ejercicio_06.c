#include <stdio.h>
#define FIL 3
#define COL 3


void imprimir_vectores(int *vector)
{
    unsigned int i = 0;

    for(i=0; i<FIL; i++)
        printf(" \t %5d \n", vector[i]);

}


int main(void)
{
    unsigned int i = 0, j = 0, k = 0;
    int aux = 0;

    int u[COL] = {1, 2, 3};
    int v[COL] = {6, 5, 4};

    int A[FIL][COL] = {{1, 5, 0},{7, 1, 2},{0, 0, 1}};
    int B[FIL][COL] = {{-2, 0, 1},{1, 0, 0},{4, 1, 0}};

    int w[FIL], x[FIL], y[FIL], z[FIL];
    int C[FIL][COL], D[FIL][COL];


    printf("\n1. w = u - 3v:\n\n");

    for(i=0; i<COL; i++)
    {
        w[i] = u[i] - 3*v[i];
    }

    imprimir_vectores(&w[0]);


    printf("\n2. w = u - v:\n\n");

    for(i=0; i<COL; i++)
    {
        x[i] = u[i] - v[i];
    }

    imprimir_vectores(&x[0]);


    printf("\n3. y = Au: \n\n");

    for(i=0; i<FIL; i++)
    {
        aux = 0;

        for(j=0; j<COL; j++)
        {
            aux += A[i][j]*u[j];
        }

        y[i] = aux;
    }

    imprimir_vectores(&y[0]);


    printf("\n4. z = Au - v: \n\n");

    for(i = 0; i<FIL; i++)
    {
        z[i] = y[i] - v[i];
    }

    imprimir_vectores(&z[0]);


    printf("\n5. C = 4A -3B: \n\n");

    for(i=0; i<FIL; i++)
    {
        for(j=0; j<COL; j++)
        {
            C[i][j] = 4*A[i][j] - 3*B[i][j];
        }
    }

    for(i=0; i<FIL; i++)
    {
        for(j=0; j<COL; j++)
        {
            printf("\t %5d", C[i][j]);
        }

        printf("\n");
    }


    printf("\n6. D = AB: \n\n");

    for(i=0; i<FIL; i++)
    {
        for(j=0; j<COL; j++)
        {

            aux = 0;

            for(k=0; k<COL; k++)
            {
                aux += A[i][k]*B[k][j];
            }

            D[i][j] = aux;

        }
    }

    for(i=0; i<FIL; i++)
    {
        for(j=0; j<COL; j++)
        {
            printf("\t %5d", D[i][j]);
        }

        printf("\n");
    }


    return 0;
}
