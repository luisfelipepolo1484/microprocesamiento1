#include<stdio.h>


float cuadrado(float x);


int main(void)
{
    float num;
    float num_square = 0.0;


    printf("Ingrese un número decimal:\n");
    scanf("%f", &num);

    num_square = cuadrado(num);

    printf("\n%f es el cuadrado de %f.\n", num_square, num);


    return 0;
}


float cuadrado(float x)
{
    return x*x;
}
