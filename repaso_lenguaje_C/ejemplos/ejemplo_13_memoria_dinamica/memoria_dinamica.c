#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    int i = 0;
    int finc = 0;

    float step = 0.05;

    float x = 1000;
    float *p_x;


    finc = (int)(x/step);

    p_x = (float *)malloc(finc*sizeof(float));

    for(i=0;i<finc;i++)
    {
        x += step;
        p_x[i] = x;
    }


    for(i=0;i<finc;i++)
    {
        printf("%f\n", p_x[i]);
    }


    free(p_x);

    return 0;
}
