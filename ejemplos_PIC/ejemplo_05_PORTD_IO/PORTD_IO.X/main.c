#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{
    PORTD = 0x00;
    TRISD = 0x88;
    
    while(1)
    {
        if(RD3 == 0)
        {
            __delay_ms(100);

            if(RD3 == 0)
            {
              PORTD = 0x55;
              __delay_ms(100);
            }
        }
        
        if(RD7 == 0)
        {
            __delay_ms(100);

            if(RD7 == 0)
            {
              PORTD = 0x22;
              __delay_ms(100);
            }
        }
    }
    
    return 0;
}
